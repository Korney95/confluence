**docker file для help.web-canape.ru**

установить docekr-compose: 
https://gist.github.com/EvgenyOrekhov/1ed8a4466efd0a59d73a11d753c0167b

1) **Создать диретории для монтирования данных из контейнера:**
mkdir confluence db

2) запуск контейнера mysql и смена пароля производится следующим образом: 
shell> docker logs mysql1 2>&1 | grep GENERATED
GENERATED ROOT PASSWORD: THIS IS ROOT PASSWORD

3) Смена стандартного пароля производится так : 
mysql> ALTER USER 'root'@'localhost' IDENTIFIED BY 'password';

4) Создание бд для confluence: 

CREATE DATABASE confluence CHARACTER SET utf8 COLLATE utf8_bin;

CREATE USER 'confluence'@'%' IDENTIFIED WITH mysql_native_password 
BY 'password';

GRANT ALL ON confluence.* TO 'confluence'@'%';

5) При установке confluence выбираем внешняя база mysql и выбираем тип подключения сторка:

jdbc:mysql://dockermysqlhost:3306/databasename?autoReconnect=true&useSSL=false